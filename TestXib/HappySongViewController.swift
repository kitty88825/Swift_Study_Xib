//
//  HappySongViewController.swift
//  TestXib
//
//  Created by 謝欣岑 on 2018/7/1.
//  Copyright © 2018年 謝欣岑. All rights reserved.
//

import UIKit

class HappySongViewController: SongViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        songLabels[0].text = "有點甜"
        songLabels[1].text = "愛的主旋律"
        songLabels[2].text = "戀愛頻率"
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
