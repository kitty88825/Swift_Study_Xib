//
//  ViewController.swift
//  TestXib
//
//  Created by 謝欣岑 on 2018/7/1.
//  Copyright © 2018年 謝欣岑. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBAction func happyButton(_ sender: UIButton) {
        performSegue(withIdentifier: "SelectToHappyView", sender: nil)
    }
    
    @IBAction func sadButton(_ sender: UIButton) {
        performSegue(withIdentifier: "SelectToSadView", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

